//
//  main.swift
//  proj1
//
//  Created by Filip Klembara on 17/02/2020.
//

import Foundation
import FiniteAutomata
import Simulator

// MARK: - Main
func main() -> Result<Void, RunError> {
    if CommandLine.arguments.count < 3
    {
        return .failure(.IncorrectArgs);
    }
    
    let inputString = CommandLine.arguments[1];
    let inputFileName = CommandLine.arguments[2];
        
    do
    {
        let jsonString = try String(contentsOfFile: inputFileName, encoding: .utf8);
        
        if let data = jsonString.data(using: .utf8)
        {
            do
            {
                let finiteAutomata = try JSONDecoder().decode(FiniteAutomata.self, from: data);
                //print(finiteAutomata);
                
                let simulator = Simulator.init(finiteAutomata: finiteAutomata);
                
                if !simulator.checkStates()
                {
                    return .failure(.UndefinedState);
                }
                
                if !simulator.checkSymbols()
                {
                    return .failure(.UndefinedSymbol);
                }
                
                if !simulator.checkDeterminism()
                {
                    return .failure(.UndetermisticAutomata);
                }
                
                let simulationResult = simulator.simulate(on: inputString);
                
                if simulationResult.count == 0
                {
                    return .failure(.StringNotAccepted);
                }
                
                for item in simulationResult {
                    print(item);
                }
            }
            catch
            {
                return .failure(.DecodeError);
            }
        }
        else
        {
            return .failure(.DecodeError);
        }
    }
    catch
    {
        return .failure(.FileError);
    }
    
    return result;
}

// MARK: - program body
let result = main()

switch result {
case .success:
    break
case .failure(let error):
    var stderr = STDERRStream()
    print("Error:", error.description, to: &stderr)
    exit(Int32(error.code))
}
