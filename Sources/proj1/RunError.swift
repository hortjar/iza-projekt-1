//
//  RunError.swift
//  proj1
//
//  Created by Filip Klembara on 17/02/2020.
//

enum RunError: Error {
    case StringNotAccepted;
    case IncorrectArgs;
    case FileError;
    case DecodeError;
    case UndefinedState;
    case UndefinedSymbol;
    case UndetermisticAutomata;
    case OtherError;
}

// MARK: - Return codes
extension RunError {
    var code: Int {
        switch self {
        case .StringNotAccepted:
            return 6;
        case .IncorrectArgs:
            return 11;
        case .FileError:
            return 12;
        case .DecodeError:
            return 20;
        case .UndefinedState:
            return 21;
        case .UndefinedSymbol:
            return 22;
        case .UndetermisticAutomata:
            return 23;
        case .OtherError:
            return 99;
        }
    }
}

// MARK:- Description of error
extension RunError: CustomStringConvertible {
    var description: String {
        switch self {
        case .StringNotAccepted:
            return "String is not accepted by automata";
        case .IncorrectArgs:
            return "Incorrect arguments";
        case .FileError:
            return "Could not open file";
        case .DecodeError:
            return "Could not decode automata";
        case .UndefinedState:
            return "Automata contains undefined state";
        case .UndefinedSymbol:
            return "Automata contains undefined symbol";
        case .UndetermisticAutomata:
            return "Automata is not determistic";
        case .OtherError:
            return "Other error occured";
        }
    }
}
